from django.shortcuts import render
from .models import AutomobileVO, Sale, Salesperson, Customer
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .encoders import SalesPersonEncoder, CustomerEncoder, SaleEncoder

@require_http_methods(["GET"])
def api_list_salesperson(request):
        salesperson = Salesperson.objects.all()
        return JsonResponse({"salesperson": salesperson}, encoder = SalesPersonEncoder )

@require_http_methods(["POST"])
def api_create_salesperson(request):
        content = json.loads(request.body)
        try:
             salespeople = Salesperson.objects.create(**content)
             return JsonResponse(salespeople, encoder = SalesPersonEncoder, safe=False)
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "This employee does not exist"}, status = 404)


@require_http_methods(["GET", "DELETE","PUT"])
def api_show_saleperson(request, pk):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            return JsonResponse(salesperson, encoder = SalesPersonEncoder, safe = False)
        except Salesperson.DoesNotExist:
             return JsonResponse({"message": "This employee does not exist"}, status = 404)

    elif request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse(salesperson, encoder= SalesPersonEncoder, safe=False)
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "This employee does not exist"}, status = 404)
    else:
        content = json.loads(request.body)
        try:
            Salesperson.objects.filter(id=pk).update(**content)
            salesperson = Salesperson.objects.get(id=pk)
            return JsonResponse(salesperson, encoder = SalesPersonEncoder, safe = False)
        except:
            return JsonResponse({"message": "This employee does not exist"}, status=404)


@require_http_methods(["GET"])
def api_list_customer(request):
        customers = Customer.objects.all()
        return JsonResponse({"customers": customers}, encoder = CustomerEncoder )

@require_http_methods(["POST"])
def api_create_customer(request):
        content = json.loads(request.body)
        try:
             customers = Customer.objects.create(**content)
             return JsonResponse(customers, encoder = CustomerEncoder, safe=False)
        except Customer.DoesNotExist:
            return JsonResponse({"message": "I'm sorry, we have no record of this customer."}, status = 404)

@require_http_methods(["GET", "DELETE","PUT"])
def api_show_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(customer, encoder = CustomerEncoder, safe = False)
        except Customer.DoesNotExist:
             return JsonResponse({"message": "I'm sorry, we have no record of this customer."}, status = 404)

    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(customer, encoder= CustomerEncoder, safe=False)
        except Customer.DoesNotExist:
            return JsonResponse({"message": "I'm sorry, we have no record of this customer."}, status = 404)
    else:
        content = json.loads(request.body)
        try:
            Customer.objects.filter(id=pk).update(**content)
            customer = Customer.objects.get(id=pk)
            return JsonResponse(customer, encoder = CustomerEncoder, safe = False)
        except:
            return JsonResponse({"message": "Does not exist"}, status=404)

@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return  JsonResponse({"sales": sales},encoder = SaleEncoder)
    else:
        content = json.loads(request.body)
        try:
            print(content)
            employee_number = content["salesperson"]
            print(employee_number)
            salesperson = Salesperson.objects.get(id=employee_number)
            print(salesperson)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "There is no Salesperson matching that employee number"} , status = 404)
        try:
            customer_name = content["customer"]
            customer = Customer.objects.get(id=customer_name)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse({"message": "There is no Customer matching this name in our records"} , status = 404)
        try:
            vin = content["car"]
            car = AutomobileVO.objects.get(vin=vin)
            content["car"] = car
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message" "We're sorry, there is no Vin number matching in our records"} , status = 404)
        sale = Sale.objects.create(**content)
        return JsonResponse(sale, encoder=SaleEncoder, safe=False)
        #except:
            #return JsonResponse({"message": "There was an error"}, status = 404)



@require_http_methods(["GET",])
def api_show_employee_sale(request, employee_number):
    try:
        sale = Sale.objects.filter(salesperson__employee_number=employee_number)
        return JsonResponse(
            {'sale':sale},
            encoder=SaleEncoder,
            )
    except Sale.DoesNotExist:
        return JsonResponse(
            {'error': 'Sale not found'},
            status=404,)

@require_http_methods(["GET", "DELETE","PUT"])
def api_show_sale(request, pk):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=pk)
            return JsonResponse(sale, encoder = SaleEncoder, safe = False)
        except Sale.DoesNotExist:
             return JsonResponse({"message": "I'm sorry, we have no record of this sale."}, status = 404)
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse(sale, encoder = SaleEncoder, safe=False)
        except Sale.DoesNotExist:
            return JsonResponse({"message": "I'm sorry, we have no record of this customer."}, status = 404)
    else:
        content = json.loads(request.body)
        print(content)
        try:
            Sale.objects.filter(id=pk).update(**content)
            sale = Sale.objects.get(id=pk)
            return JsonResponse(sale, encoder = SaleEncoder, safe = False)
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=404)
