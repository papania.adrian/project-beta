from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    import_href = models.CharField(max_length=200)

class Salesperson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=200, unique=True)


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=20)

class Sale(models.Model):
    price = models.PositiveSmallIntegerField()
    salesperson = models.ForeignKey(Salesperson,
    related_name="salesperson",
    on_delete = models.CASCADE
    )

    car = models.ForeignKey(AutomobileVO,
    related_name = "car",
    on_delete = models.CASCADE)

    customer = models.ForeignKey(Customer,
    related_name="customer",
    on_delete = models.CASCADE)
