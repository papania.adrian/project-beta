from common.json import ModelEncoder
from .models import Salesperson, Sale, AutomobileVO, Customer

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["id", "vin", "import_href"]

class SalesPersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ['id', 'name', 'employee_number']

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "name", "address", "phone"]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ["id","price", "salesperson", "car", "customer"]

    encoders = {
        "car" : AutomobileVOEncoder(),
        "salesperson" : SalesPersonEncoder(),
        "customer" : CustomerEncoder(),
    }
