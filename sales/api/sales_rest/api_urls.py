from django.urls import path
from .views import api_list_salesperson, api_show_saleperson, api_list_customer, api_show_customer, api_list_sales, api_show_employee_sale, api_show_sale, api_create_salesperson, api_create_customer
urlpatterns = [
    path('salespeople/', api_list_salesperson, name ="api_show_salespeople"),
    path('salespeople/create/', api_create_salesperson, name ="api_create_salespeople"),
    path('salespeople/<int:pk>/', api_show_saleperson, name="api_show_salesperson"),
    path('customer/', api_list_customer, name ="api_show_customers"),
    path('customer/create/', api_create_customer, name ="api_create_customers"),
    path('customer/<int:pk>/', api_show_customer, name="api_show_customer"),
    path('sales/', api_list_sales, name ="api_show_sales"),
    path('sales/employee/<employee_number>/', api_show_employee_sale, name="api_show_employee_sale"),
    path('sales/<int:pk>/', api_show_sale, name="api_show_sale"),]
