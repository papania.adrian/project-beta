from common.json import ModelEncoder
from .models import Technician, ServiceAppointment

class technician_encoder(ModelEncoder):
    model = Technician
    properties = ['name', 'id']

class technician_detail_encoder(ModelEncoder):
    model = Technician
    properties = ['name', 'employee_number', 'id']

class appointment_list_encoder(ModelEncoder):
    model = ServiceAppointment
    properties = ['id',
        'vin',
        'owner_name',
        'date_time',
        'service_needed',
        'service_completed',
        'assigned_tech',]
    encoders = {'assigned_tech': technician_encoder()}
