from django.db import models

# Create your models here.


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    import_href = models.CharField(max_length=200)


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.IntegerField()

    def __str__(self):
        return self.name

class ServiceAppointment(models.Model):
    vin = models.CharField(max_length=100)
    owner_name = models.CharField(max_length=100)
    date_time = models.DateTimeField()
    service_needed = models.CharField(max_length=200)
    service_completed = models.BooleanField(default=False)
    assigned_tech = models.ForeignKey(
        Technician,
        on_delete=models.CASCADE,
        related_name="technician",
    )

    def __str__(self):
        return self.owner_name
