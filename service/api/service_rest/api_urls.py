from django.urls import path
from .views import api_list_appointments, api_create_technician, api_show_technicians, api_edit_service, api_show_service, api_edit_technician


urlpatterns = [
    path('service/', api_list_appointments, name='api_list_appointments'),
    path('technician/', api_show_technicians, name='api_show_technicians'),
    path('technician/create/', api_create_technician, name='api_create_technician'),
    path('service/<int:pk>/', api_edit_service, name='api_edit_service'),
    path('service-history/<vin>/', api_show_service, name='api_show_service'),
    path('technician-edit/<int:pk>/', api_edit_technician, name='api_edit_technician')
]
