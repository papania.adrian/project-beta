from django.shortcuts import render
from .models import Technician, ServiceAppointment
from django.http import JsonResponse
import json
from .encoders import technician_detail_encoder, appointment_list_encoder
from django.views.decorators.http import require_http_methods
# Create your views here.

@require_http_methods(['POST'])
def api_create_technician(request):
    # Create tech
    content = json.loads(request.body)
    tech = Technician.objects.create(**content)
    return JsonResponse(tech, encoder=technician_detail_encoder, safe=False)

@require_http_methods(["GET"])
def api_show_technicians(request):
        tech = Technician.objects.all()
        return JsonResponse(
            {'tech': tech},
            encoder=technician_detail_encoder)

@require_http_methods(["GET", "DELETE", "PUT"])
def api_edit_technician(request, pk):
    if request.method == "GET":
        try:
            tech = Technician.objects.get(pk=pk)
            return JsonResponse(tech, encoder=technician_detail_encoder, safe=False)
        except Technician.DoesNotExist:
            return JsonResponse(
                {"error": "Technician not found"},
            )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            Technician.objects.filter(id=pk).update(**content)
            service = Technician.objects.get(id=pk)
            return JsonResponse(
                service,
                encoder=technician_detail_encoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"error": "Technician not found"},
            )

    else:
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", 'POST'])
def api_list_appointments(request):
    if request.method == 'GET':
        appointment = ServiceAppointment.objects.all()
        return JsonResponse(
            {'appointments': appointment},
            encoder=appointment_list_encoder,)

    else:#Create a new appointment
        content = json.loads(request.body)

        try:
            tech = content['assigned_tech']
            technician= Technician.objects.get(id=tech)
            content['assigned_tech'] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {'error': 'Technician not found'},
                status=400,
            )

        appointment = ServiceAppointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=appointment_list_encoder,
            safe=False,)

@require_http_methods(["DELETE","GET", "PUT"])
def api_edit_service(request, pk):
    if request.method == "DELETE":
        count, _ = ServiceAppointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    elif request.method == "GET":
        try:
            service = ServiceAppointment.objects.get(id=pk)
            return JsonResponse(
                service,
                encoder=appointment_list_encoder,
                safe=False,
                )
        except ServiceAppointment.DoesNotExist:
                    return JsonResponse(
                    {"error": "Service not found"},)

    else:
        content = json.loads(request.body)
        ServiceAppointment.objects.filter(id=pk).update(**content)
        service = ServiceAppointment.objects.get(id=pk)
        return JsonResponse(
            service,
            encoder=appointment_list_encoder,
            safe=False,
        )

@require_http_methods(["GET"])
def api_show_service(request, vin):
    try:
        service = ServiceAppointment.objects.filter(vin=vin)
        return JsonResponse(
            {'service':service},
            encoder=appointment_list_encoder,
            )
    except ServiceAppointment.DoesNotExist:
        return JsonResponse(
            {'error': 'Service not found'},
            status=400,)
