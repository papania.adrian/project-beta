from django.contrib import admin
from .models import Technician, ServiceAppointment
# Register your models here.



@admin.register(Technician)
class Technician(admin.ModelAdmin):
    pass

@admin.register(ServiceAppointment)
class ServiceAppointment(admin.ModelAdmin):
    pass
