# CarCar

Team:

* Adrian Papania - Service Microservice
* Nicolas Gallo - Sales Microservice

## Design

## Service microservice

The service microservice models are ServiceAppointment, Technician, and AutomobileVO. They are used to creat a service appointment, and to make a technician. With the vin, owners name and type of service needed you are able to create a service appointment. With the Technician model you are able to create a Technician to assign to the service appointment. With the AutomobileVO model you are able to use to in the polling service. With having a vin I was also able to have a VIP status when an appointment was made using the same vin that was sold in the inventory.

## Sales microservice

The Sales microservice models were handled in such a way that I created a VO of the Automobile model in inventory that utilized VIN number and an import_href. This connects the automobile in inventory with the VO in the Sales microservice models through a poller in the Sales Microservice which retrieves automobiles through the inventory. I also included a Salesperson model to keep track of the employees selling vehicles, on that model was the name of the employee and their employee_number (which can be used to search for a list of all sales made by that employee). The Customer model accepts a name, address, and phone number to keep record of the customers of the car dealership. The Sales model takes the name of the sales person, the name of the customer, and links it to an automobile's vin number to match (all 3 afforementioned fields as foreign keys), and has a price field to list the cost of the vehicle. All four models were critical in establishing a proper sales record, which would be useful for our car dealership in their record keeping.
