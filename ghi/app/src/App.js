import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianList from './Services/TechnicianList';
import TechnicianForm from './Services/TechnicianForm';
import AppointmentList from './Services/AppointmentList';
import AppointmentForm from './Services/AppointmentForm';
import ServiceHistory from './Services/ServiceHistory';
import AutomobileList from './InventoryJava/AutomobileList';
import AutomobileForm from './InventoryJava/AutomobileForm';
import VehicleModelList from './InventoryJava/VehicleModelList';
import VehicleModelForm from './InventoryJava/VehicleModelForm';
import ManufacturerList from './InventoryJava/ManufacturerList';
import ManufacturerForm from './InventoryJava/ManufacturerForm';
import SalesList from './Sales/SalesList';
import SalespersonForm from './Sales/SalespersonForm';
import SalespersonList from './Sales/SalespersonList';
import CustomerList from './Sales/CustomerList';
import CustomerForm from './Sales/CustomerForm';
import SalespersonSalesHistory from './Sales/SalespersonSalesHistory';
import SalesForm from './Sales/SalesForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technician">
            <Route path="" element={<TechnicianList techs={props.techs} />} />
            <Route path="create" element={<TechnicianForm />} />
          </Route>
          <Route path="service">
            <Route path="" element={<AppointmentList service={props.service} />} />
            <Route path="create" element={<AppointmentForm />} />
          </Route>
          <Route path="service-history">
            <Route path="" element={<ServiceHistory service={props.vin} />} />
            <Route path="create" element={<ServiceHistory />} />
          </Route>
          <Route path="automobiles">
            <Route path="" element={<AutomobileList service={props.vin} />} />
            <Route path="create" element={<AutomobileForm />} />
          </Route>
          <Route path="models">
            <Route path="" element={<VehicleModelList service={props.name} />} />
            <Route path="create" element={<VehicleModelForm />} />
          </Route>
          <Route path="manufacturers">
            <Route path="" element={<ManufacturerList service={props.name} />} />
            <Route path="create" element={<ManufacturerForm />} />
          </Route>
          <Route path="sales">
            <Route path="" element={<SalesList service={props.sales} />} />
            <Route path="create" element={<SalesForm />} />
            <Route path="employee" element={<SalespersonSalesHistory service={props.sale} />} />
          </Route>
          <Route path="salespeople">
            <Route path="" element={<SalespersonList service={props.salesperson} />} />
            <Route path="create" element={<SalespersonForm />} />
          </Route>
          <Route path="customer">
            <Route path="" element={<CustomerList service={props.customer} />} />
            <Route path="create" element={<CustomerForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
