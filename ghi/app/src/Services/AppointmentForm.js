import React from "react";


class AppointmentForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vin: '',
            owner_name: '',
            date_time: '',
            service_needed: '',
            service_completed: false,
            assigned_tech: '',
            techs: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleVinChange = this.handleVinChange.bind(this);
        this.handleOwnerNameChange = this.handleOwnerNameChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleServiceChange = this.handleServiceChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    async componentDidMount() {
        const url = 'http://localhost:8080/api/technician/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ techs: data.tech })
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.techs;
        const serviceUrl = 'http://localhost:8080/api/service/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(serviceUrl, fetchConfig);





        if (response.ok) {



            this.setState({
                vin: '',
                owner_name: '',
                date_time: '',
                service_needed: '',
                service_completed: false,
                assigned_tech: '',
            });
            window.location = '/service'
        };
    }
    handleVinChange(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }

    handleOwnerNameChange(event) {
        const value = event.target.value;
        this.setState({ owner_name: value });
    }

    handleDateChange(event) {
        const value = event.target.value;
        this.setState({ date_time: value });
    }
    handleServiceChange(event) {
        const value = event.target.value;
        this.setState({ service_needed: value });
    }

    handleChange(event) {
        const value = event.target.value;
        this.setState({ assigned_tech: value });
    }
    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label htmlFor="vin">Vin</label>
                        <input onChange={this.handleVinChange} value={this.state.vin} required type="text" name="manufacturer" id="manufacturer" className="form-control" />

                    </div>
                    <div>
                        <label htmlFor="owner_name">Owner Name</label>
                        <input onChange={this.handleOwnerNameChange} value={this.state.owner_name} required type="text" name="model_name" id="model_name" className="form-control" />
                    </div>
                    <div>
                        <label htmlFor="date-time">Date and Time</label>
                        <input onChange={this.handleDateChange} value={this.state.date_time} required type="text" name="model_name" id="model_name" className="form-control" />
                    </div>
                    <div>
                        <label htmlFor="service_needed">Service Needed</label>
                        <input onChange={this.handleServiceChange} value={this.state.service_needed} required type="text" name="model_name" id="model_name" className="form-control" />
                    </div>
                    <div>
                        <label>Technician</label>
                        <select onChange={this.handleChange} required name="technician" id="technicain" className="form-select">
                            <option value="">Choose a Technician</option>
                            {this.state.techs.map(tech => {
                                return (
                                    <option key={tech.id} value={tech.id}>{tech.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <button>Create Appointment</button>
                </form>
            </div>

        );
    }
}
export default AppointmentForm;
