import {useState, useEffect} from 'react';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';




function ServiceList() {
    const[Services, SetService] = useState([]);

    const getData = async () => {
        const resp = await fetch('http://localhost:8080/api/service/')
        const data = await resp.json();
        SetService(data.appointments);
    }

    const handleDelete = async (id) => {
            const resp = await fetch(`http://localhost:8080/api/service/${id}`, {method:"DELETE"})
            getData();
        }



    const handleEdit = async (id) => {
        const service_completed = true
        const data = {...id, service_completed}
        data.assigned_tech=data.assigned_tech.id


        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const appointmentId = id.id
        const resp = await fetch(`http://localhost:8080/api/service/${appointmentId}/`, fetchConfig)
        getData();
    }

    const [automobiles, SetAutomobiles] = useState([]);

    const getAutomobiles = async () => {
        const resp = await fetch('http://localhost:8100/api/automobiles/')
        const data = await resp.json();
        SetAutomobiles(data.autos);
    }

    const showVIP = (vin) => {
        if (automobiles.find((car) => car.vin === vin)){
            return {
                backgroundColor: "green",
            }
        }
    }



    useEffect(() => {
        getData();
        getAutomobiles();
    }, [])

    return (
        <table className="table table-striped">
            <thead>Appointment List
                <tr>
                    <th>Vin</th>
                    <th>Owner Name</th>
                    <th>Date and Time</th>
                    <th>Service Needed</th>
                    <th>Assigned Technician</th>
                </tr>
            </thead>
        <tbody>
        {
            Services.filter(status=>(status.service_completed===false)).map(appointments=> {
                return(<tr key={appointments.id} style={showVIP(appointments.vin)}>
                    <td>{appointments.vin}</td>
                    <td>{appointments.owner_name}</td>
                    <td>{appointments.date_time}</td>
                    <td>{appointments.service_needed}</td>
                    <td>{appointments.assigned_tech.name}</td>
                    <td><button onClick={() => handleDelete(appointments.id)}>Cancel</button></td>
                    <td><button onClick={() => handleEdit(appointments)}>Finished</button></td>
                </tr>)
            })
        }<Link to="/service/create"><button>Create Appointment</button></Link>
        </tbody>
        </table>

)

}
export default ServiceList
