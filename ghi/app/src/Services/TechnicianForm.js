import React from "react";


class TechnicianForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            employee_number: ''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmployeeNumberChange = this.handleEmployeeNumberChange.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.tech;
        const techUrl = 'http://localhost:8080/api/technician/create/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(techUrl, fetchConfig);

        if (response.ok) {


            this.setState({
                name: '',
                employee_number: '',
            });
            window.location = '/technician'

        };
    }
// = (event) => {

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }

    handleEmployeeNumberChange(event) {
        const value = event.target.value;
        this.setState({ employee_number: value });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label htmlFor="name">Name</label>
                        <input onChange={this.handleNameChange} value={this.state.name} required type="text" name="manufacturer" id="name" className="form-control" />

                    </div>
                    <div>
                        <label htmlFor="employee_number">Employee Number</label>
                        <input onChange={this.handleEmployeeNumberChange} value={this.state.employee_number} required type="text" name="employee_number" id="employee_number" className="form-control" />

                    </div>
                    <button>Create Technician</button>
                </form>
            </div>

        );
    }
}
export default TechnicianForm;
