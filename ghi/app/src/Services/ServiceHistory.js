import {useState, useEffect} from 'react';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';




function ServiceListHistory() {
    const[Services, SetService] = useState([]);
    const[FilteredServices, SetFilteredService] = useState([]);
    const getServices = async () =>{
        const resp = await fetch('http://localhost:8080/api/service/')
        const data = await resp.json();

        SetService(data.appointments);
    }

    const search = async (event) =>{


        event.preventDefault();
        const vin = event.target.vin.value
        console.log('vin:', vin)
        let filtered = []
        if (vin==''){
        }
        else{

        console.log(Services)
        filtered = Services.filter(value=>{
            return (value.vin===vin);

        })};


        SetFilteredService(filtered);







    }



    useEffect(() => {
        getServices();

    }, [])

    return (
        <div>
            <form onSubmit={(event)=>search(event)}>
                <div>
                    <h1>Service History</h1>
                    <input id="vin" name="vin"/>
                    <button type="submit">Search</button>
                </div>
            </form>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Owner Name</th>
                        <th>Date and Time</th>
                        <th>Service Needed</th>
                        <th>Assigned Technician</th>
                        <th>Service Completed</th>
                    </tr>
                </thead>
            <tbody>
            {
                FilteredServices.map(history=> {
                    return(<tr key={history.id}>
                        <td>{history.vin}</td>
                        <td>{history.owner_name}</td>
                        <td>{history.date_time}</td>
                        <td>{history.service_needed}</td>
                        <td>{history.assigned_tech.name}</td>
                        <td>{history.service_completed.toString()}</td>
                    </tr>)
                })
            }
            </tbody>
            </table>
        </div>

    )

}
export default ServiceListHistory;
