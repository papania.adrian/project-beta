import {useState, useEffect} from 'react';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';




function TechnicianList() {
    const[Techs, SetTechs] = useState([]);



    useEffect(() => {
        const getData = async () => {
            const resp = await fetch('http://localhost:8080/api/technician/')
            const data = await resp.json();
            SetTechs(data.tech);
        }
        getData();
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>employee_number</th>
                </tr>
            </thead>
        <tbody>
        {
            Techs.map(Tech=> {
                return(<tr key={Tech.id}>
                    <td>{Tech.name}</td>
                    <td>{Tech.employee_number}</td>
                </tr>)
            })
        }<Link to="/technician/create"><button>Create Technician</button></Link>
        </tbody>
        </table>

)

}
export default TechnicianList
