import {useState, useEffect} from 'react';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';




function ModelList() {
    const[Models, SetCar] = useState([]);



    useEffect(() => {
        const getData = async () => {
            const resp = await fetch('http://localhost:8100/api/models/')
            const data = await resp.json();
            SetCar(data.models);
        }
        getData();
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
        <tbody>
        {
            Models.map(car=> {
                return(<tr key={car.id}>
                    <td>{car.name}</td>
                    <td>{car.manufacturer.name}</td>
                    <td><img src={car.picture_url}width="100px" length= "100px"/></td>
                </tr>)
            })
        }<Link to="/models/create"><button>Create Model</button></Link>
        </tbody>
        </table>

)

}
export default ModelList
