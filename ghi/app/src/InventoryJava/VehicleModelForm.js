import React from "react";


class VehicleModelForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            picture_url: '',
            manufacturer: '',
            manufacturers: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePicURLChange = this.handlePicURLChange.bind(this);
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        console.log(response)
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            this.setState({manufacturers: data.manufacturers})
        }
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.manufacturers;
        const carUrl = 'http://localhost:8100/api/models/'
        console.log(data);
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };


        const response = await fetch(carUrl, fetchConfig);

        if (response.ok) {


            this.setState({
                name: '',
                picture_url: '',
                manufacturer: '',
            });
            window.location = '/models'

        };
    }
// = (event) => {

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }

    handlePicURLChange(event) {
        const value = event.target.value;
        this.setState({ picture_url: value });
    }

    handleManufacturerChange(event) {
        const value = event.target.value;
        console.log(value)
        this.setState({ manufacturer_id: value });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label htmlFor="name">Name</label>
                        <input onChange={this.handleNameChange} value={this.state.name} required type="text" name="manufacturer" id="name" className="form-control" />
                    </div>
                    <div>
                        <label htmlFor="picture_url">Picture URL</label>
                        <input onChange={this.handlePicURLChange} value={this.state.picture_url} required type="text" name="picture_url" id="name" className="form-control" />
                    </div>
                    <div>
                    <label>Manufacturer</label>
                        <select onChange={this.handleManufacturerChange} required name="manufacturers" id="manufacturers" className="form-select">
                            <option value="">Choose a Manufacturer</option>
                            {this.state.manufacturers.map(make => {
                                return (
                                    <option key={make.id} value={make.id}>{make.name}</option>
                                )
                            })}
                        </select>

                    </div>
                    <button>Create Vehicle</button>
                </form>
            </div>

        );
    }
}
export default VehicleModelForm;
