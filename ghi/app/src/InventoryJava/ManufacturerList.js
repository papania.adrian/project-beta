import {useState, useEffect} from 'react';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';




function ManufacturerList() {
    const[Manufacturer, SetCar] = useState([]);



    useEffect(() => {
        const getData = async () => {
            const resp = await fetch('http://localhost:8100/api/manufacturers/')
            const data = await resp.json();
            SetCar(data.manufacturers);
        }
        getData();
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
        <tbody>
        {
            Manufacturer.map(car=> {
                return(<tr key={car.id}>
                    <td>{car.name}</td>
                </tr>)
            })
        }<Link to="/manufacturers/create"><button>Create Manufacturer</button></Link>
        </tbody>
        </table>

)

}
export default ManufacturerList
