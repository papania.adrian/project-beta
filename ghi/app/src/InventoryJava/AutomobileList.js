import {useState, useEffect} from 'react';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';




function AutomobileList() {
    const[Models, SetCar] = useState([]);



    useEffect(() => {
        const getData = async () => {
            const resp = await fetch('http://localhost:8100/api/automobiles/')
            const data = await resp.json();
            SetCar(data.autos);
        }
        getData();
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Vin</th>
                    <th>Model</th>
                </tr>
            </thead>
        <tbody>
        {
            Models.map(car=> {
                return(<tr key={car.id}>
                    <td>{car.color}</td>
                    <td>{car.year}</td>
                    <td>{car.vin}</td>
                    <td>{car.model.name}</td>
                </tr>)
            })
        }<Link to="/automobiles/create"><button>Create Automobile</button></Link>
        </tbody>
        </table>

)

}
export default AutomobileList
