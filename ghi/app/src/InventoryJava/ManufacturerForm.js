import React from "react";


class ManufacturerForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.manufacturer;
        const carUrl = 'http://localhost:8100/api/manufacturers/'
        console.log(carUrl);
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(carUrl, fetchConfig);

        if (response.ok) {


            this.setState({
                name: '',
            });
            window.location = '/manufacturers'

        };
    }
// = (event) => {

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label htmlFor="name">Name</label>
                        <input onChange={this.handleNameChange} value={this.state.name} required type="text" name="manufacturer" id="name" className="form-control" />

                    </div>
                    <button>Create Manufacturer</button>
                </form>
            </div>

        );
    }
}
export default ManufacturerForm;
