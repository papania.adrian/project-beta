import React from "react";


class AutomobileForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            color: '',
            year: '',
            vin: '',
            model:'',
            models: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleYearChange = this.handleYearChange.bind(this);
        this.handleVinChange = this.handleVinChange.bind(this);
        this.handleModelChange = this.handleModelChange.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        console.log(response)
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            this.setState({models: data.models})
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.models;
        const carUrl = 'http://localhost:8100/api/automobiles/'
        console.log(data);
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(carUrl, fetchConfig);

        if (response.ok) {


            this.setState({
                color: '',
                year: '',
                vin: '',
                model:'',
            });
            window.location = '/automobiles'

        };
    }
// = (event) => {

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }
    handleYearChange(event) {
        const value = event.target.value;
        this.setState({ year: value });
    }
    handleVinChange(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }
    handleModelChange(event) {
        const value = event.target.value;
        this.setState({ model_id: value });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label htmlFor="color">Color</label>
                        <input onChange={this.handleColorChange} value={this.state.name} required type="text" name="manufacturer" id="name" className="form-control" />
                    </div>
                    <div>
                        <label htmlFor="year">Year</label>
                        <input onChange={this.handleYearChange} value={this.state.name} required type="text" name="manufacturer" id="name" className="form-control" />
                    </div>
                    <div>
                        <label htmlFor="vin">Vin</label>
                        <input onChange={this.handleVinChange} value={this.state.name} required type="text" name="manufacturer" id="name" className="form-control" />
                    </div>
                        <label>Model</label>
                            <select onChange={this.handleModelChange} required name="automibiles" id="automobiles" className="form-select">
                                <option value="">Choose a Model</option>
                                {this.state.models.map(make => {
                                    return (
                                        <option key={make.id} value={make.id}>{make.name}</option>
                                    )
                                })}
                            </select>
                    <button>Create Automobile</button>
                </form>
            </div>

        );
    }
}
export default AutomobileForm;
