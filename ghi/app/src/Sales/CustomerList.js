import { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';




function CustomerList() {
    const [Customer, SetCustomer] = useState([]);



    useEffect(() => {
        const getData = async () => {
            const resp = await fetch('http://localhost:8090/api/customer/')
            const data = await resp.json();
            SetCustomer(data.customers);
        }
        getData();
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Customer Name</th>
                    <th>Address</th>
                    <th>Phone Number</th>
                </tr>
            </thead>
            <tbody>
                {
                    Customer.map(customer => {
                        return (<tr key={customer.id}>
                            <td>{customer.name}</td>
                            <td>{customer.address}</td>
                            <td>{customer.phone}</td>
                        </tr>)
                    })
                }<Link to="/customer/create"><button>Create Customer</button></Link>
            </tbody>
        </table>

    )

}
export default CustomerList
