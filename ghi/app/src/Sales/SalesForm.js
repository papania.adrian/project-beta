import React from "react";


class SalesForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            price: "",
            salesperson: "",
            automobilevo: [],
            car: "",
            salespersons: [],
            customer: "",
            customers: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleAutomobileVOChange = this.handleAutomobileVOChange.bind(this);
        this.handleSalespersonChange = this.handleSalespersonChange.bind(this);
        this.handleCustomerChange = this.handleCustomerChange.bind(this);
        this.handlePriceChange = this.handlePriceChange.bind(this);
    }

    async componentDidMount() {
        const salespersonurl = 'http://localhost:8090/api/salespeople/';
        const salespersonresponse = await fetch(salespersonurl);
        if (salespersonresponse.ok) {
            const salespersondata = await salespersonresponse.json();
            this.setState({ salespersons: salespersondata.salesperson })
        }
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ automobilevo: data.autos })
        }
        const customerurl = 'http://localhost:8090/api/customer/';
        const customerresponse = await fetch(customerurl);
        if (customerresponse.ok) {
            const customerdata = await customerresponse.json();
            this.setState({ customers: customerdata.customers })
        }

    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.salespersons;
        delete data.customers;
        delete data.automobilevo;
        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(salesUrl, fetchConfig);

        if (response.ok) {

            alert("Sale created successfully");

            this.setState({
                car: '',
                salesperson: '',
                customer: '',
                price: '',
            });

        };
    }


    handleAutomobileVOChange(event) {
        const value = event.target.value;
        console.log(value)
        this.setState({ car: value });
    }

    handleSalespersonChange(event) {
        const value = event.target.value;
        console.log(value)
        this.setState({ salesperson: value });

    }

    handleCustomerChange(event) {
        const value = event.target.value;
        this.setState({ customer: value });
    }

    handlePriceChange(event) {
        const value = event.target.value;
        this.setState({ price: value });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label>Automobile</label>
                        <select onChange={this.handleAutomobileVOChange} required name="Car" id="Car" className="form-select" > <option value="">Choose a Car</option>
                            {this.state.automobilevo.map(car => {
                                return (
                                    <option key={car.vin} value={car.vin}>{car.vin}</option>
                                )
                            })}
                        </select>
                    </div>
                    <div>
                        <label>Salesperson</label>
                        <select onChange={this.handleSalespersonChange} required name="Salesperson" id="Salesperson" className="form-select" > <option value="">Choose a Salesperson</option>
                            {this.state.salespersons.map(person => {
                                return (
                                    <option key={person.id} value={person.id}>{person.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <div>
                        <label>Customer</label>
                        <select onChange={this.handleCustomerChange} required name="Customer" id="Customer" className="form-select" > <option value="">Choose a Customer</option>
                            {this.state.customers.map(customer => {
                                return (
                                    <option key={customer.id} value={customer.id}>{customer.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <div>
                        <label htmlFor="price">Price</label>
                        <input onChange={this.handlePriceChange} value={this.state.model_name} required type="text" name="model_name" id="model_name" className="form-control" />

                    </div>
                    <button>Create Sale</button>
                </form>
            </div>

        );
    }
}
export default SalesForm;
