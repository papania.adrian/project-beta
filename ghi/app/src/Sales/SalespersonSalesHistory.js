import { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';




function SalespersonSalesHistory() {
    const [SalespersonsSalesHistory, SetSalespersonSalesHistory] = useState([]);
    const [FilteredSalesHistory, SetSalesHistory] = useState([]);
    const getSalesHistory = async () => {
        const resp = await fetch('http://localhost:8090/api/sales/')
        const data = await resp.json();
        console.log(data);
        SetSalespersonSalesHistory(data.sales);
    }

    const search = async (event) => {


        event.preventDefault();
        const employee_number = event.target.employee_number.value
        let filtered = []
        if (employee_number == '') {
        }
        else {
            console.log(SalespersonsSalesHistory)
            filtered = SalespersonsSalesHistory.filter(value => {
                return (value.salesperson.employee_number === employee_number);

            })
        };


        SetSalesHistory(filtered);







    }



    useEffect(() => {
        getSalesHistory();

    }, [])

    return (
        <div>
            <form onSubmit={(event) => search(event)}>
                <div>
                    <h1>Employee Sales History</h1>
                    <input id="employee_number" name="employee_number" />
                    <button type="submit">Search</button>
                </div>
            </form>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>Vin</th>
                        <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        FilteredSalesHistory.map(SalesHistory => {
                            return (<tr key={SalesHistory.id}>
                                <td>{SalesHistory.salesperson.name}</td>
                                <td>{SalesHistory.customer.name}</td>
                                <td>{SalesHistory.car.vin}</td>
                                <td>{SalesHistory.price}</td>
                            </tr>)
                        })
                    }
                </tbody>
            </table>
        </div>

    )

}
export default SalespersonSalesHistory;
