import React from "react";


class SalespersonForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            employee_number: ''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmployeeNumberChange = this.handleEmployeeNumberChange.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.sales;
        const salespersonUrl = 'http://localhost:8090/api/salespeople/create/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(salespersonUrl, fetchConfig);

        if (response.ok) {

            alert("Salesperson created successfully");

            this.setState({
                name: '',
                employee_number: '',
            });

        };
    }


    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }

    handleEmployeeNumberChange(event) {
        const value = event.target.value;
        this.setState({ employee_number: value });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label htmlFor="name">Name</label>
                        <input onChange={this.handleNameChange} value={this.state.model_name} required type="text" name="model_name" id="model_name" className="form-control" />

                    </div>
                    <div>
                        <label htmlFor="employee_number">Employee Number</label>
                        <input onChange={this.handleEmployeeNumberChange} value={this.state.model_name} required type="text" name="model_name" id="model_name" className="form-control" />

                    </div>
                    <button>Create Salesperson</button>
                </form>
            </div>

        );
    }
}
export default SalespersonForm;
