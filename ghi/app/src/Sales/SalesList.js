import { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';




function SalesList() {
    const [Sales, SetSales] = useState([]);



    useEffect(() => {
        const getData = async () => {
            const resp = await fetch('http://localhost:8090/api/sales/')
            const data = await resp.json();
            console.log(data);
            SetSales(data.sales);
        }
        getData();
    }, [])

    return (
        <table className="table table-striped">
            <thead>Sales List
                <tr>
                    <th>Employee Name</th>
                    <th>Employee Number</th>
                    <th>Customer</th>
                    <th>Vin Number</th>
                    <th>Sale Price</th>
                </tr>
            </thead>
            <tbody>
                {
                    Sales.map(sales => {
                        return (<tr key={sales.id}>
                            <td>{sales.salesperson.name}</td>
                            <td>{sales.salesperson.employee_number}</td>
                            <td>{sales.customer.name}</td>
                            <td>{sales.car.vin}</td>
                            <td>{sales.price}</td>
                        </tr>)
                    })
                }<Link to="/sales/create/"><button>Create a Sale</button></Link>
            </tbody>
        </table>

    )

}
export default SalesList
