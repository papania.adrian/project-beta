import { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';




function SalespersonList() {
    const [Salesperson, SetSalesperson] = useState([]);



    useEffect(() => {
        const getData = async () => {
            const resp = await fetch('http://localhost:8090/api/salespeople/')
            const data = await resp.json();
            SetSalesperson(data.salesperson);
        }
        getData();
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>employee_number</th>
                </tr>
            </thead>
            <tbody>
                {
                    Salesperson.map(salesperson => {
                        return (<tr key={salesperson.id}>
                            <td>{salesperson.name}</td>
                            <td>{salesperson.employee_number}</td>
                        </tr>)
                    })
                }<Link to="/salespeople/create"><button>Create Salesperson</button></Link>
            </tbody>
        </table>

    )

}
export default SalespersonList
