import React from "react";


class CustomerForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            address: '',
            phone: ''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleAddressChange = this.handleAddressChange.bind(this);
        this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.customers;
        const customerUrl = 'http://localhost:8090/api/customer/create/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(customerUrl, fetchConfig);

        if (response.ok) {

            alert("Customer created successfully");

            this.setState({
                name: '',
                address: '',
                phone: '',
            });

        };
    }


    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }

    handleAddressChange(event) {
        const value = event.target.value;
        this.setState({ address: value });
    }

    handlePhoneNumberChange(event) {
        const value = event.target.value;
        this.setState({ phone: value });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label htmlFor="name">Name</label>
                        <input onChange={this.handleNameChange} value={this.state.model_name} required type="text" name="model_name" id="model_name" className="form-control" />

                    </div>
                    <div>
                        <label htmlFor="address">Address</label>
                        <input onChange={this.handleAddressChange} value={this.state.model_name} required type="text" name="model_name" id="model_name" className="form-control" />

                    </div>
                    <div>
                        <label htmlFor="phone_number">Phone Number</label>
                        <input onChange={this.handlePhoneNumberChange} value={this.state.model_name} required type="text" name="model_name" id="model_name" className="form-control" />

                    </div>
                    <button>Create Customer</button>
                </form>
            </div>

        );
    }
}
export default CustomerForm;
